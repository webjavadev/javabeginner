package composition;

/**
 * @author john.
 * @since 23.09.2016 at 11:23.
 */
class Door
{
    void open()
    {
        System.out.println("Door was open");
    }

    void close()
    {
        System.out.println("Door was closed");
    }
}
