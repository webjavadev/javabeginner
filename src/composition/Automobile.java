package composition;

/**
 * Пример делегирования, вызовы методов переадресуются встроенному обьекту engine и door
 * а интерфейс остаётся как при наследовании.
 * @author john.
 * @since 22.09.2016 at 22:13.
 */
public class Automobile
{
    Engine engine = new Engine();
    Door door = new Door();
    void start()
    {
        engine.start();
    }
    void stop()
    {
        engine.stop();
    }
    void open()
    {
        door.open();
    }
    void close()
    {
        door.close();
    }
}
