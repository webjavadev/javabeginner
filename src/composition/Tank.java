package composition;

/**
 * Пример композиции, вызовы методов переадресуются встроенному обьекту engine и door
 * а интерфейс не остаётся как при наследовании.
 * @author john.
 * @since 22.09.2016 at 22:13.
 */
public class Tank
{
    Engine engine = new Engine();
    Door door = new Door();

    public Tank()
    {
    }
}
