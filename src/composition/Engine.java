package composition;

/**
 * @author john.
 * @since 22.09.2016 at 22:11.
 */
class Engine
{
    void start()
    {
        System.out.println("Engine: start()");
    }
    void stop()
    {
        System.out.println("Engine: stop()");
    }
}
