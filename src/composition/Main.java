package composition;

/**
 * Пример делегирования, вызовы методов переадресуются встроенным обьектам engine и door
 * а интерфейс остаётся как при наследовании.
 * @author john.
 * @since 22.09.2016 at 22:14.
 */
public class Main
{
    public static void main(String[] args)
    {
        Automobile automobile = new Automobile();
        automobile.start();
        automobile.stop();
        automobile.open();
        automobile.close();
        automobile.engine.start();
        Tank tank = new Tank();
        tank.engine.start();
//        tank.start(); не могу вызвать старт, потому что не определён такой метод.
    }
}
/**
 Engine: start()
 Engine: stop()
 Door was open
 Door was closed
 Engine: start()
 Engine: start()
 */
