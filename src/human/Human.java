package human;

import java.time.LocalDate;
import java.time.Period;

/**
 * Created by john on 29.08.2016.
 * @author john
 * This class create a new object - Human with 4 params
 */
public class Human
{
    private String firstName; // FirstName of Human
    private String lastName; // LastName of Human
    private int age; //age of the Human
    private LocalDate birthday; // date of birth of Human

    /**
     * Construct a object Human with arguments: FN = null, LN = null, Age = 0 & Date of Birthday = 1900.01.01
     */
    public Human()
    {
        this.firstName = null;
        this.lastName = null;
        this.age = 0;
        this.birthday = LocalDate.of(1900,1,1);
    }

    /**
     * Construct a object Human with arguments: FN = FN, LN = null, Age = 0 & Date of Birthday = 1900.01.01
     * @param firstName must be a string
     */
    public Human(String firstName)
    {
        this.firstName = clear(firstName);
        this.lastName = null;
        this.age = 0;
        this.birthday = LocalDate.of(1900,1,1);
    }

    /**
     * Construct a object Human with arguments: FN = FN, LN = LN, Age = 0 & Date of Birthday = 1900.01.01
     */
    public Human(String firstName, String lastName)
    {
        this.firstName = clear(firstName);
        this.lastName = clear(lastName);
        this.age = 0;
        this.birthday = LocalDate.of(1900,1,1);
    }

    /**
     * Construct a object Human with arguments: FN = null, LN = null, Age = age & Date of Birthday = 1900.01.01
     */
    public Human(int age)
    {
        this.firstName = null;
        this.lastName = null;
        this.age = age;
        this.birthday = getBirthday(age);
    }

    /**
     * Construct a object Human with arguments: FN = null, LN = null, Age = 0 & Date of Birthday = year.month.date
     */
    public Human(int year, int month, int date)
    {
        this.firstName = null;
        this.lastName = null;
        this.age = getAge(year,month,date);
        this.birthday = LocalDate.of(year,month,date);
    }

    /**
     * Construct a object Human with arguments: FN = FN, LN = LN, Age = age & Date of Birthday = calculated from age
     */
    public Human(String firstName, String lastName, int age)
    {
        this.firstName = clear(firstName);
        this.lastName = clear(lastName);
        this.age = age;
        this.birthday = getBirthday(age);
    }

    /**
     * Construct a object Human with arguments: FN = FN, LN = LN, Age = calculated from BD &
     * Date of Birthday = year.month.date
     */
    public Human(String firstName, String lastName, int year, int month, int date)
    {
        this.firstName = clear(firstName);
        this.lastName = clear(lastName);
        this.age = getAge(year, month, date);
        this.birthday = LocalDate.of(year,month,date);
    }

    /**
     * Construct a object Human with arguments: FN = FN, LN = LN, Age = age & Date of Birthday = year.month.date
     */
    public Human(String firstName, String lastName, int age, int year, int month, int date)
    {
        this.firstName = clear(firstName);
        this.lastName = clear(lastName);
        this.age = age;
        this.birthday = LocalDate.of(year,month,date);
    }

    /**
     * Clone a object Human with arguments of input Human:
     * FN = FN, LN = LN, Age = age & Date of Birthday = year.month.date
     */
    public Human(Human human)
    {
        this.firstName = human.firstName;
        this.lastName = human.lastName;
        this.age = human.age;
        this.birthday = human.birthday;
    }

    /**
     * @return the FN
     */
    public String getFirstName()
    {
        return this.firstName;
    }

    /**
     * @return the LN
     */
    public String getLastName()
    {
        return this.lastName;
    }

    /**
     * Set the FN
     * @param firstName
     */
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    /**
     * Set the LN
     * @param lastName
     */
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    /**
     *
     * @param year
     * @param month
     * @param date
     * @return int of the age
     */
    public int getAge(int year, int month, int date)
    {
        Period p = Period.between(LocalDate.of(year,month,date),LocalDate.now());
        return p.getYears();
    }

    /**
     *
     * @param ld
     * @return int of the age from LocalDate
     */
    public int getAge(LocalDate ld)
    {
        return ld.getYear();
    }
    /**
     * Set the Age from int
     * @param year
     */
    public void setAge(int year)
    {
        this.age = year;
    }

    /**
     *
     * @param age
     * @return the LocalDate of Birthday calculated from the age
     */
    public LocalDate getBirthday(int age)
    {
        return LocalDate.now().minusYears(age);
    }

    /**
     * Set the BD
     * @param year
     * @param month
     * @param date
     */
    public void setBirthday(int year, int month, int date)
    {
        this.birthday = LocalDate.of(year,month,date);
    }

    /**
     * Set the BD
     * @param ld
     */
    public void setBirthday(LocalDate ld)
    {
        this.birthday = ld;
    }

    /**
     * Print the arguments of the Human
     */
    public void print()
    {
        System.out.println("FirstName is - " + getFirstName());
        System.out.println("LastName is - " + getLastName());
        System.out.println("was born at " + this.birthday + " and is " + this.age + " years old");
//        System.out.println("was born at " + this.birthday + " and is " + getAge(this.birthday.getYear(),this.birthday.getMonthValue(),this.birthday.getDayOfMonth()) + " years old");
    }

    /**
     * Correct the FN & LN
     */
    private void correctString()
    {
        String pattern = "[^A-Za-zА-Яа-я]+";

        if (!this.firstName.isEmpty())
        {
            this.firstName = this.firstName.trim();
            this.firstName = this.firstName.replaceAll(pattern, "");
            this.firstName = this.firstName.substring(0,1).toUpperCase() + this.firstName.substring(1).toLowerCase();
        }

        if (!this.lastName.isEmpty())
        {
            this.lastName = this.lastName.trim();
            this.lastName = this.lastName.replaceAll(pattern, "");
            this.lastName = this.lastName.substring(0,1).toUpperCase() + this.lastName.substring(1).toLowerCase();
        }
    }

    /**
     * Correct the FN & LN
     */
    public void correct()
    {
        this.setFirstName(clear(firstName));
        this.setLastName(clear(lastName));
    }

    /**
     * Clear the input and leave only Engine-Za-zА-Яа-я characters
     * @param str
     * @return new cleared str
     */
    private static String clear(String str)
    {
        if (!str.isEmpty())
        {
            String pattern = "[^A-Za-zА-Яа-я]+";
            str = str.trim();
            str = str.replaceAll(pattern, "");
            str = str.substring(0,1).toUpperCase() + str.substring(1).toLowerCase();
        }
        return str;
    }
}
