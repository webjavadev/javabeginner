package human;

/**
 * @author john
 *         Created on 12.09.2016 at 15:20.
 */
public class User implements People
{
String stt;

    @Override
    public String getParam()
    {
        System.out.println("User.getParam.stt = " + stt);
        return this.stt;
    }

    @Override
    public void setParam(String s)
    {
        System.out.println("Begin User.setParam.stt = " + stt);
        this.stt = s;
        System.out.println("End User.setParam.stt = " + stt);
    }

    public void setClear()
    {
        stt = null;
    }
}
