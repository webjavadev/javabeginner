package human;

/**
 * @author john
 *         Created on 12.09.2016 at 15:20.
 */
public class Client implements People
{
String stt;

    @Override
    public String getParam()
    {
        System.out.println("Client.getParam.stt = " + stt);
        return this.stt;
    }

    @Override
    public void setParam(String s)
    {
        System.out.println("Begin Client.setParam.stt = " + stt);
        this.stt = s;
        System.out.println("End Client.setParam.stt = " + stt);
    }
}
