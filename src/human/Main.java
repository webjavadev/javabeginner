package human;

import java.util.Date;
//import static net.mindview.util.Print.*;
import static java.lang.Integer.valueOf;

/**
 * Created by john on 01.09.2016.
 */
public class Main
{
    public int level;
    public static void main(String[] args)
    {
        try
        {
            System.out.println(new Date());
//            System.getProperties().list(System.out);
            System.out.println("Hi " + System.getProperty("user.name") + ". You are running on " + System.getProperty("sun.desktop"));
            Main m1 = new Main();
            Main m2 = new Main();
            m1.level = 5;
            m2.level = 3;
            System.out.println(m1.level + " " + m2.level);
//            m1.level = valueOf(m2.level);
            m1.level = m2.level;
            System.out.println(m1.level + " " + m2.level);
            System.out.println(m1.level == m2.level);
            System.out.println(m1.equals(m2));
            m1.level = 7;
            System.out.println(m1.level + " " + m2.level);

            int x = 1;
            int z = 2;
            System.out.println(x + " " + z);
            x = z;
            System.out.println(x + " " + z);
            x = 7;
            System.out.println(x + " " + z);
            int y = 5 / x;
        }
        catch (Exception e)
        {
            System.out.println("Exception");
        }
//        catch (ArithmeticException ae)
//        {
//            System.out.println("Arithmetic Exception");
//        }
        System.out.println("finished");
    }
}
