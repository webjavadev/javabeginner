package human;

/**
 * @author john
 *         Created on 12.09.2016 at 15:27.
 */
public class Main1
{
    public static void main(String[] args)
    {
        People p1 = new User();
        People p2 = new Client();
//        People p3;
//        People p4;
        p1.setParam("ppp1");
        p2.setParam("ppp2");
        printPeople(p1);

        User u1 = new User();
        u1.setParam("uuu1");
        p1 = u1;
        printPeople(p1);
        p2 = u1;
        printPeople(p2);

//        p3 = p1;
//        p4 = p2;
//        p3.getParam();
//        p3 = p2;
//        p3.getParam();


//        p4.getParam();
    }

public static void printPeople(People p)
{
    People p3;
    p3 = p;
    String st = p3.getParam();

    System.out.println("Print People " + st);
}

}
