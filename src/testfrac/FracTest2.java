package testfrac;

import frac.Frac;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.rules.Timeout;

import static org.junit.Assert.*;

/**
 * @author john.
 * @since 24.09.2016 at 10:24.
 */
public class FracTest2
{
    private Frac ab1;
    private Frac ab2;
    private Frac ab3;

    @Rule // TestFrac must execute in less than 1 second
    public Timeout timeout = Timeout.seconds(1);

    @Rule
    public final ExpectedException arithmeticException = ExpectedException.none();

    @Before
    public void setUp() throws Exception
    {
        ab1 = new Frac();
        ab2 = new Frac(1,2);
        ab3 = new Frac();
    }

    @After
    public void tearDown() throws Exception
    {

    }

    @org.junit.Test
    public void ttoString() throws Exception
    {
        String expected = "0/1";
        assertEquals(expected, ab1.toString());
    }

    @Test
    public void getNumerator() throws Exception
    {
        assertEquals(0, ab1.getNumerator());
    }

    @Test
    public void getDenominator() throws Exception
    {
        assertEquals(1, ab1.getDenominator());
    }

    @Test
    public void setNumerator() throws Exception
    {
        ab1.setNumerator(2);
        assertEquals(2, ab1.getNumerator());
    }

    @Test
    public void setDenominator() throws Exception
    {
        ab1.setDenominator(2);
        assertEquals(2, ab1.getDenominator());
    }
    @Test
    public void setDenominatorZero() throws Exception
    {
        arithmeticException.expect(ArithmeticException.class);
        ab1.setDenominator(0);
    }

    @Test
    public void add() throws Exception
    {
        ab3 = ab1.add(ab2);
        assertEquals(1, ab3.getNumerator());
        assertEquals(2, ab3.getDenominator());
        assertEquals("1/2", ab3.toString());
        assertTrue(ab3 != ab1);
    }

    @Test
    public void add1() throws Exception
    {

    }

    @Test
    public void minus() throws Exception
    {

    }

    @Test
    public void minus1() throws Exception
    {

    }

    @Test
    public void intMinus() throws Exception
    {

    }

    @Test
    public void mul() throws Exception
    {

    }

    @Test
    public void mul1() throws Exception
    {

    }

    @Test
    public void divInt() throws Exception
    {
        ab2 = ab1.div(1);
        assertEquals(ab2.toString(), ab1.toString());
    }

    @Test
    public void divIntZero() throws Exception
    {
        try
        {
            ab2 = ab1.div(0);
            Assert.fail("Should be an exception!");
        }
        catch (ArithmeticException ex)
        {

        }
    }

    @Test (expected = ArithmeticException.class)
    public void divIntZeroEx() throws Exception
    {
//        arithmeticException.expect(ArithmeticException.class);
        ab2 = ab1.div(0);
    }

    @Test
    public void divFrac() throws Exception
    {
        ab3 = ab1.div(ab2);
        assertEquals(ab3.toString(), ab1.toString());
    }

    @Test
    public void intDiv() throws Exception
    {

    }

    @Test
    public void simple() throws Exception
    {

    }

    @Test
    public void setFrac() throws Exception
    {

    }

    @Test
    public void setFrac1() throws Exception
    {

    }

    @Test
    public void setFrac2() throws Exception
    {

    }

    @Test
    public void turn() throws Exception
    {

    }

    @Test
    public void turnCurrent() throws Exception
    {

    }

    @Test
    public void power() throws Exception
    {

    }

    @Test
    public void isEqual() throws Exception
    {

    }

    @Test
    public void isOver() throws Exception
    {

    }

    @Test
    public void isUnder() throws Exception
    {

    }

    @Test
    public void isOverEqual() throws Exception
    {

    }

    @Test
    public void isUnderEqual() throws Exception
    {

    }

    @Test
    public void inFrac() throws Exception
    {

    }

    @Test
    public void inPositive() throws Exception
    {

    }

    @Test
    public void printFrac() throws Exception
    {

    }

    @Test
    public void inFracStr() throws Exception
    {

    }

}