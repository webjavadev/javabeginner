package calculate;

/**
 * @author john.
 * @since 21.09.2016 at 20:10.
 */
public interface MN
{
    // All methods automatically are public, and we don't need to define it here, but in class we must
    // define them as public

    /**
     * Get the current number
     * @return number
     */
    MN get();

    /**
     * Get the current number in string format
     * @return String
     */
    String getString();

    /**
     * Set the current number
     * @param mn - number
     */
    void set(MN mn);

    /**
     * Set the current number
     * @param str - String
     */
    void set(String str);

    /**
     * Input the number
     */
    void input();

    /**
     * Convert current number to String
     * @return - String
     */
    String toString(); // Not need, because we already have this method in class Object

    /**
     * Print the current number
     */
    void print();

    /**
     * Add giving number to current number
     * @param mn - number
     * @return - number
     */
    MN add(MN mn);

    /**
     * Subtract giving number from current number
     * @param mn - number
     * @return - number
     */
    MN subtract(MN mn);

    /**
     * Multiply giving number with current number
     * @param mn - number
     * @return - number
     */
    MN multiply(MN mn);

    /**
     * Divide current number to giving number
     * @param mn - number
     * @return - number
     */
    MN div(MN mn);

    /**
     * Compare if the values of the objects are equals
     * @param mn - number
     * @return {@code true} if values are equals, or {@code false}
     */
    boolean equals(MN mn);

}
