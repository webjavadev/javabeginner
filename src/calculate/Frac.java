package calculate;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * @author john.
 * @since 22.09.2016 at 12:07.
 */
public class Frac implements MN
{
    /**
     * Числитель, он же numerator
     */
    private int numerator;
    /**
     * Знаменатель, он же denominator
     */
    private int denominator;

    /**
     * конструктор вставляет числитель 0 а в знаменателе 1
     */
    public Frac()
    {
        this.numerator = 0;
        this.denominator = 1;
    }

    /**
     * конструктор вставляет числитель входящее число, а в знаменателе 1
     * @param numerator - Числитель
     */
    public Frac(int numerator)
    {
        this.numerator = numerator;
        this.denominator = 1;
    }

    /**
     * конструктор вставляет числитель первый аргумент а в знаменателе второй аргумент
     * @param numerator - Числитель
     * @param denominator - Знаменатель, не должен быть нулём
     */
    public Frac(int numerator, int denominator)
    {
        if(denominator == 0)
        {
            throw new ArithmeticException("Denominator can't be equals to zero!");
        }
        Integer absnum = Math.abs(numerator);
        Integer absden = Math.abs(denominator);
        if (!absnum.equals(1) || !absden.equals(1))
        {
            int m = Math.abs(numerator);
            int n = denominator;
            if (m < n)
            {
                int z = m;
                m = n;
                n = z;
            }
            int r = m % n;
            while (r != 0 && n != 1)
            {
                m = n;
                n = r;
                r = m % n;
            }
            if (n != 0 && n != 1)
            {
                numerator /= n;
                denominator /= n;
            }
        }
        if (denominator < 0)
        {
            numerator *= -1;
            denominator *= -1;
        }
        this.numerator = numerator;
        this.denominator = denominator;
    }

    /**
     * Конструктор, который принимает в качестве входящего параметра дробь
     * Клонирует, делает копию оригинальной дроби
     * @param original - дробь
     */
    public Frac(Frac original)
    {
        this.numerator = original.numerator;
        this.denominator = original.denominator;
    }

    public Frac(String str)
    {
        try {
            String[] frac = str.split("(\\/)");
            this.numerator = Integer.parseInt(frac[0]);
            this.denominator = Integer.parseInt(frac[1]);
            this.simple();
        }catch( Exception e ) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * В текущую дробь делает копию входной дроби
     * @param frac - входящая дробь
     */
    void setFrac(Frac frac)
    {
        this.numerator = frac.numerator;
        this.denominator = frac.denominator;
    }

    /**
     * возвращает значение числителя
     * @return - Числитель
     */
    public int getNumerator()
    {
        return this.numerator;
    }

    /**
     * возвращает значение знаменателя
     * @return - знаменатель
     */
    public int getDenominator()
    {
        return this.denominator;
    }

    /**
     * Возвращает значение текущей дроби
     * @return - обьект MN
     */
    @Override
    public MN get()
    {
        MN mn = new Frac(this);
        return mn;
    }

    /**
     * Возвращает текущую дробь в виде строки
     * @return - строка
     */
    @Override
    public String getString()
    {
        return this.toString();
    }

    @Override
    public void set(String str)
    {
        try {
            String[] frac = str.split("(\\/)");
            this.numerator = Integer.parseInt(frac[0]);
            this.denominator = Integer.parseInt(frac[1]);
        }catch( Exception e ) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * В текущую дробь подставляет входящее число
     * @param mn - входящее число
     */
    @Override
    public void set(MN mn)
    {
//        this.numerator = ((Frac) mn).numerator;
//        this.denominator = ((Frac) mn).denominator;
        Frac a = (Frac) mn;
        this.numerator = a.numerator;
        this.denominator = a.denominator;
    }

    /**
     * Ввод дроби
     */
    @Override
    public void input()
    {
        Scanner input = new Scanner(System.in);
        System.out.print("Please enter the integer number as numerator of the Frac: \n");
        this.numerator = input.nextInt();
        System.out.print("Please enter the integer number as denominator of the Frac: \n");
        this.denominator = inPositive();
        this.simple();
    }

    /**
     * Ввод дроби в виде дроби
     */
//    @Override
    public void inputFrac()
    {
        Scanner input = new Scanner(System.in);
        int i = 0;
        System.out.println("Please enter a Frac, f/e: 1/2 or -1/2!");
        while (!input.hasNext("(\\-*\\d+\\/[1-9]+)") && i < 3) {
            System.out.println("That's not a valid Frac!");
            input.next();
            i++;
        }
        this.set(input.next());
        this.simple();
        System.out.println("Thank you! Got " + this);
    }

    /**
     * проверяет если число больше нуля
     * @return - целое число больше нуля
     */
    private static int inPositive()
    {
        Scanner sc = new Scanner(System.in);
        int positiveNumber;
        do {
            System.out.println("Please enter a positive number!");
            while (!sc.hasNextInt())
            {
                System.out.println("That's not a number!");
                sc.next(); // this is important!
            }
            positiveNumber = sc.nextInt();
        } while (positiveNumber <= 0);
        return positiveNumber;
    }

    /**
     * переводит дробь в строку в следующем формате "числитель/знаменатель"
     * @return - Числитель/Знаменатель
     */
    @Override
    public String toString()
    {
        String str = null;
        try
        {
            if (this.denominator < 0)
            {
                str = "-" + this.numerator + "/" + Math.abs(this.denominator);
            } else
            {
                str = +this.numerator + "/" + this.denominator;
            }
        }
        catch (Exception e)
        {
            System.out.println("Can't out toString" + e);
        }
        return str;
    }

    /**
     * вывод дроби на экран
     */
    @Override
    public void print()
    {
        try
        {
            System.out.print(this.toString() + " ");
        }
        catch (NullPointerException e)
        {
            System.out.println("Can't print null Frac! " + e.getMessage());
        }
        catch (Exception e2)
        {
            System.out.println("Can't print your Frac! " + e2.getMessage());
        }
    }

    /**
     * возвращает сумму дроби + входной дроби
     * @param mn - входящая дробь
     * @return - Сумма двух дробей
     */
    @Override
    public MN add(MN mn)
    {
        Frac f = (Frac) mn;
        return new Frac(this.numerator * f.denominator + this.denominator * f.numerator, this.denominator * f.denominator);
//        int nn = this.numerator * frac.denominator + this.denominator * frac.numerator;
//        int nd = this.denominator * frac.denominator;
//        Frac nF = new Frac(nn, nd);
//        nF.simple();
//        return new Frac(nn, nd);
    }

    /**
     * возвращает разницу текущей дроби и входящей
     * @param mn - входящая дробь
     * @return - дробь
     */
    @Override
    public MN subtract(MN mn)
    {
        Frac f = (Frac) mn;
        return new Frac(this.numerator * f.denominator - this.denominator * f.numerator, this.denominator * f.denominator);
//        int nn = this.numerator * frac.denominator - this.denominator * frac.numerator;
//        int nd = this.denominator * frac.denominator;
//        Frac nF = new Frac(nn, nd);
//        nF.simple();
//        return nF;
    }

    /**
     * возвращает произведение дроби * входной дроби
     * @param mn - входящая дробь
     * @return - дробь
     */
    @Override
    public MN multiply(MN mn)
    {
        Frac f = (Frac) mn;
        return new Frac(this.numerator * f.numerator, this.denominator * f.denominator);
//        int nn = this.numerator * frac.numerator;
//        int nd = this.denominator * frac.denominator;
//        Frac nF = new Frac(nn, nd);
//        nF.simple();
//        return nF;
    }

    /**
     * возвращает деление дроби / входной дроби
     * @param mn - входящая дробь
     * @return - дробь
     */
    @Override
    public MN div(MN mn)
    {
        Frac f = (Frac) mn;
        return new Frac(this.numerator * f.denominator, this.denominator * f.numerator);
//        int nn = this.numerator * frac.denominator;
//        int nd = this.denominator * frac.numerator;
//        Frac nF = new Frac(nn, nd);
//        nF.simple();
//        return nF;
    }

    /**
     * проверяет если дробь равна с входной дробью
     * @param mn - входящая дробь
     * @return - {@code true} если значения равны, иначе {@code false}
     */
    @Override
    public boolean equals(MN mn)
    {
        return (this.numerator) * ((Frac) mn).getDenominator() == ((Frac) mn).getNumerator() * (this.denominator);
    }

    /**
     * упрощение дроби до простой
     */
    private void simple()
    {
        if (this.denominator != 1 && this.numerator != 1)
        {
            int m = Math.abs(this.numerator);
            int n = this.denominator;
            if (m < n)
            {
                int z = m;
                m = n;
                n = z;
            }
            int r = m % n;
            while (r != 0 && n != 1)
            {
                m = n;
                n = r;
                r = m % n;
            }
            if (n != 0 && n != 1)
            {
                this.numerator = this.numerator / n;
                this.denominator = this.denominator / n;
            }
        }
    }
}