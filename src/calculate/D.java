package calculate;

import java.util.Scanner;

import static java.lang.Double.parseDouble;

/**
 * @author john.
 * @since 29.09.2016 at 20:46.
 */
public class D implements MN
{
    private Double d;

    /**
     * Constructs a newly allocated {@code D} object that
     * represents the specified {@code Double} value.
     */
    public D()
    {
        this.d = 0.0;
    }

    /**
     * Constructs a newly allocated {@code D} object that
     * represents the specified {@code Double} value.
     * @param d - the {@code Double} to be converted to an
     *          {@code Double}.
     */
    public D(Double d)
    {
        this.d = d;
    }

    /**
     * Get the current number
     *
     * @return number
     */
    @Override
    public MN get()
    {
        return new D(this.d);
    }

    /**
     * Returns the {@code double} value of this {@code Double} object.
     *
     * @return the {@code double} value represented by this object
     */
    public Double getD()
    {
        return this.d;
    }

    /**
     * Get the current number in string format
     *
     * @return String
     */
    @Override
    public String getString()
    {
        return this.toString();
    }

    /**
     * Set the current number
     *
     * @param mn - number
     */
    @Override
    public void set(MN mn)
    {
        D db = (D) mn;
        this.d = db.d;
    }

    /**
     * Set the current number
     *
     * @param str - String
     */
    @Override
    public void set(String str)
    {
        this.d = parseDouble(str);
    }

    /**
     * Input the number
     */
    @Override
    public void input()
    {
        Scanner input = new Scanner(System.in);
        System.out.print("Please enter the Double number as Double: \n");
        int i = 0;
        while (!input.hasNext("(-?\\d+\\.\\d+)") && i < 3) {
            System.out.println("That's not a valid Double!");
            input.next();
            i++;
        }
        this.set(input.next());
    }

    /**
     * Print the current number
     */
    @Override
    public void print()
    {
        System.out.println(this.d);
    }

    /**
     * Add giving number to current number
     *
     * @param mn - number
     * @return - number
     */
    @Override
    public MN add(MN mn)
    {
        return new D (this.d + ((D) mn).getD());
    }

    /**
     * Subtract giving number from current number
     *
     * @param mn - number
     * @return - number
     */
    @Override
    public MN subtract(MN mn)
    {
        return new D (this.d - ((D) mn).getD());
    }

    /**
     * Multiply giving number with current number
     *
     * @param mn - number
     * @return - number
     */
    @Override
    public MN multiply(MN mn)
    {
        return new D (this.d * ((D) mn).getD());
    }

    /**
     * Divide current number to giving number
     *
     * @param mn - number
     * @return - number
     */
    @Override
    public MN div(MN mn)
    {
        return new D (this.d / ((D) mn).getD());
    }

    /**
     * Compare if the values of the objects are equals
     *
     * @param mn - number
     * @return {@code true} if values are equals, or {@code false}
     */
    @Override
    public boolean equals(MN mn)
    {
        return (this.getD().doubleValue() == ((D) mn).getD().doubleValue());
    }
}
