package calculate;

import java.util.Scanner;

import static java.lang.Integer.parseInt;

/**
 * @author john.
 * @since 28.09.2016 at 18:39.
 */
public class I implements MN
{
    private Integer i;
    /**
     * Constructs a newly allocated {@code I} object that
     * represents the specified {@code int} value.
     */
    public I()
    {
        this.i = 0;
    }

    /**
     * Constructs a newly allocated {@code I} object that
     * represents the {@code int} value indicated by the
     * {@code String} parameter.
     * @param i the {@code Integer} to be converted to an
     *          {@code Integer}.
     */
    public I(Integer i)
    {
        this.i = i;
    }

    @Override
    public MN get()
    {
        MN mn = new I(this.i);
        return mn;
    }

    @Override
    public String getString()
    {
        return this.toString();
    }

    @Override
    public void set(MN mn)
    {
        I in = (I) mn;
        this.i = in.i;
    }

    @Override
    public void set(String str)throws NumberFormatException
    {
        this.i = parseInt(str, 10);
    }

    @Override
    public void input()
    {
        Scanner input = new Scanner(System.in);
        System.out.print("Please enter the integer number as Integer: \n");
        this.i = input.nextInt();
    }

    @Override
    public void print()
    {
        System.out.println(this.i);
    }

    @Override
    public MN add(MN mn)
    {
        I in = (I) mn;
        return new I (this.i + in.i);
    }

    @Override
    public MN subtract(MN mn)
    {
        I in = (I) mn;
        return new I (this.i - in.i);
    }

    @Override
    public MN multiply(MN mn)
    {
        I in = (I) mn;
        return new I (this.i * in.i);
    }

    @Override
    public MN div(MN mn)
    {
        I in = (I) mn;
        return new I (this.i / in.i);
    }

    @Override
    public boolean equals(MN mn)
    {
        return (this.get() == (I) mn.get());
    }
}
