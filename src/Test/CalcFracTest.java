package Test;

import calculate.Frac;
import calculate.MN;
import org.junit.*;
import org.junit.rules.Timeout;

import static org.junit.Assert.*;

/**
 * @author john.
 * @since 25.09.2016 at 21:00.
 */
public class CalcFracTest
{

    private Frac frac;
    private MN mn;

    @Rule // TestFrac must execute in less than 1 second
    public Timeout timeout = Timeout.seconds(1);

    @Before
    public void setUp() throws Exception
    {
        System.out.println("Before Test");
        frac = new Frac(1,2);
        mn = new Frac(frac);
    }

    @After
    public void tearDown() throws Exception
    {
        System.out.println("After test");
    }

    @Test
    public void testget() throws Exception
    {
        MN result = new Frac(1,2);
        MN aspect = new Frac(1,2);
        assertEquals(aspect, result.get());
    }

    @Test
    public void set() throws Exception
    {
        Frac a = new Frac();
        String str = "1/2";
        a.set(str);
        assertEquals("1/2", a.toString());
    }

    @Test
    public void set2() throws Exception
    {
        Frac a = new Frac();
        MN mn = new Frac(1,2);
//        Frac a = (Frac) mn;
        a.set(mn);
        assertEquals("1/2", a.toString());
    }

    @Test
    public void input() throws Exception
    {

    }

    @Test
    public void inPositive() throws Exception
    {

    }

    @Test
    public void ttoString() throws Exception
    {

    }

    @Test
    public void print() throws Exception
    {

    }

    @Test
    public void add() throws Exception
    {

    }

    @Test
    public void subtract() throws Exception
    {

    }

    @Test
    public void multiply() throws Exception
    {

    }

    @Test
    public void div() throws Exception
    {

    }

}