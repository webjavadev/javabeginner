package frac;

import java.util.Scanner;

/**
 * This Class created new object of Frac and methods to work with it
 * @author john
 * @since 18.08.2016.
 */

public class Frac
{
    /**
     * Числитель, он же numerator
     */
    private int numerator;
    /**
     * Знаменатель, он же denominator
     */
    private int denominator;

    /**
     * конструктор вставляет числитель 0 а в знаменателе 1
     */
    public Frac()
    {
        this.numerator = 0;
        this.denominator = 1;
    }

    /**
     * конструктор вставляет числитель первый аргумент а в знаменателе второй аргумент
     * @param numerator - Числитель
     * @param denominator - Знаменатель, не должен быть нулём
     */
    public Frac(int numerator, int denominator)
    {
        if(denominator == 0)
        {
            throw new ArithmeticException("Denominator can't be equals to zero!");
        } else
        {
            this.numerator = numerator;
            this.denominator = denominator;
        }
    }

    /**
     * Конструктор который принимает один параметр в качестве числителя, а в знаменателе ставит 1.
     * @param numerator - числитель
     */
    Frac(int numerator)
    {
        this.numerator = numerator;
        this.denominator = 1;
    }

    /**
     * Конструктор, который принимает в качестве входящего параметра дробь
     * Клонирует, делает копию оригинальной дроби
     * @param original - дробь
     */
    Frac(Frac original)
    {
        this.numerator = original.numerator;
        this.denominator = original.denominator;
    }

    /**
     * переводит дробь в строку в следующем формате "числитель/знаменатель"
     * @return - Числитель/Знаменатель
     */
    @Override
    public String toString()
    {
        String str = null;
        try
        {
            if (this.denominator < 0)
            {
                str = "-" + this.numerator + "/" + Math.abs(this.denominator);
            } else
            {
                str = +this.numerator + "/" + this.denominator;
            }
        }
        catch (Exception e)
        {
            System.out.println("Can't out toString" + e);
        }
        return str;
    }

    /**
     * возвращает значение числителя
     * @return - Числитель
     */
    public int getNumerator()
    {
        return this.numerator;
    }

    /**
     * возвращает значение знаменателя
     * @return - знаменатель
     */
    public int getDenominator()
    {
        return this.denominator;
    }

    /**
     * присваивает числителю значение входного параметра
     * @param numerator - число
     */
    public void setNumerator(int numerator)
    {
        this.numerator = numerator;
    }

    /**
     * присваивает знаменателю значение входного параметра
     * @param denominator - знаменатель, не должен быть нулём
     */
    public void setDenominator(int denominator)
    {
        if(denominator == 0)
        {
            throw new ArithmeticException("Denominator can't be equals to zero!");
        } else
        {
            this.denominator = denominator;
        }
    }

    /**
     * В текущую дробь делает копию входной дроби
     * @param frac - входящая дробь
     */
    void setFrac(Frac frac)
    {
        this.numerator = frac.numerator;
        this.denominator = frac.denominator;
    }

    void setFrac(int numerator, int denominator)
    {
        /* вставляет числитель первый аргумент, а в знаменателе второй аргумент*/
        if(denominator == 0)
        {
            throw new ArithmeticException("Can't set Frac. Denominator can't be equals to zero!");
        } else
        {
            this.numerator = numerator;
            this.denominator = denominator;
        }
    }
    public void setFrac(int numerator)
    {
        /* вставляет в текущую дробь в числитель входное число а в знаменателе 1 */
        this.numerator = numerator;
        this.denominator = 1;
    }
    /**
     * Ввод дроби
     */
    void inFrac()
    {
        Scanner input = new Scanner(System.in);
        System.out.print("Please enter the integer number as numerator of the Frac: \n");
        this.numerator = input.nextInt();//задаём числитель
        System.out.print("Please enter the integer number as denominator of the Frac: \n");
        this.denominator = inPositive();// задаём знаменатель
        this.simple();
    }

    /**
     * проверяет если число больше нуля
     * @return - целое число больше нуля
     */
    static int inPositive()
    {
        Scanner sc = new Scanner(System.in);
        int positiveNumber;
        do {
            System.out.println("Please enter a positive number!");
            while (!sc.hasNextInt())
            {
                System.out.println("That's not a number!");
                sc.next(); // this is important!
            }
            positiveNumber = sc.nextInt();
        } while (positiveNumber <= 0);
        return positiveNumber;
    }

    /**
     * вывод дроби на экран
     */
    void printFrac()
    {
        try
        {
            System.out.print(this.toString() + " ");
        }
        catch (NullPointerException e)
        {
            System.out.println("Can't print null Frac! " + e.getMessage());
        }
        catch (Exception e2)
        {
            System.out.println("Can't print your Frac! " + e2.getMessage());
        }
    }

    /**
     * упрощение дроби до простой
     */
    private void simple()
    {
        if (this.denominator != 1 && this.numerator != 1)
        {
            int m = Math.abs(this.numerator);
            int n = this.denominator;
            if (m < n)
            {
                int z = m;
                m = n;
                n = z;
            }
            int r = m % n;
            while (r != 0 && n != 1)
            {
                m = n;
                n = r;
                r = m % n;
            }
            if (n != 0 && n != 1)
            {
                this.numerator = this.numerator / n;
                this.denominator = this.denominator / n;
            }
        }
    }

    /**
     * ввод дроби сразу в виде дроби
     * @return - строка
     */
    public Frac inFracStr()
    {
        Scanner input = new Scanner(System.in);
        int i = 0;
        System.out.println("Please enter a Frac, f/e: 1/2!");
        while (!input.hasNext("(\\-*\\d+\\/[1-9]+)") && i < 3) {
            System.out.println("That's not a valid Frac!");
            input.next();
            i++;
        }
        String infr = input.next();
        Frac nF = new Frac();
        nF.set(infr);
        System.out.println("Thank you! Got " + infr);
        return nF;
    }

    /**
     * Переводит из строки в дробь
     * @param str - строка
     */
    public void set(String str)
    {
        try {
            String[] frac = str.split("(\\/)");
            this.numerator = Integer.parseInt(frac[0]);
            this.denominator = Integer.parseInt(frac[1]);
            this.simple();
        }catch( Exception e ) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * возвращает новую перевернутую дробь
     * @return - дробь
     */
    public Frac turn()
    {
        int nn = this.denominator;
        int nd = this.numerator;
        return new Frac(nn, nd);
    }

    /**
     * возвращает текущую дробь в перевернутом виде
     */
    public void turnCurrent()
    {
        int nn = this.denominator;
        this.denominator = this.numerator;
        this.numerator = nn;
    }

    /**
     * возвращает сумму дроби + входной дроби
     * @param frac - входящая дробь
     * @return - Сумма двух дробей
     */
    public Frac add(Frac frac)
    {
        int nn = this.numerator * frac.denominator + this.denominator * frac.numerator;
        int nd = this.denominator * frac.denominator;
        Frac nF = new Frac(nn, nd);
        nF.simple();
        return nF;
    }

    /**
     * возвращает сумму дроби + входное целое число
     * @param i - входящее целое число
     * @return - Сумма дроби
     */
    public Frac add(int i)
    {
        int nn = this.numerator + this.denominator * i;
        int nd = this.denominator;
        Frac nF = new Frac(nn, nd);
        nF.simple();
        return nF;
    }

    /**
     * возвращает разницу дроби - входной дроби
     * @param frac - входящая дробь
     * @return - разница дробей
     */
    public Frac minus(Frac frac)
    {
        int nn = this.numerator * frac.denominator - this.denominator * frac.numerator;
        int nd = this.denominator * frac.denominator;
        Frac nF = new Frac(nn, nd);
        nF.simple();
        return nF;
    }

    /**
     * возвращает разницу дроби - входное целое число
     * @param i - целое число
     * @return - дробь
     */
    public Frac minus(int i)
    {
        int nn = this.numerator - this.denominator * i;
        int nd = this.denominator;
        Frac nF = new Frac(nn, nd);
        nF.simple();
        return nF;
    }

    /**
     * возвращает разницу входное целое число - дроби
     * @param i - целое число
     * @return - дробь
     */
    public Frac intMinus(int i)
    {
        int nn = this.denominator * i - this.numerator;
        int nd = this.denominator;
        Frac nF = new Frac(nn, nd);
        nF.simple();
        return nF;
    }

    /**
     * возвращает произведение дроби * входной дроби
     * @param frac - входящая дробь
     * @return - дробь
     */
    public Frac mul(Frac frac)
    {
        int nn = this.numerator * frac.numerator;
        int nd = this.denominator * frac.denominator;
        Frac nF = new Frac(nn, nd);
        nF.simple();
        return nF;
    }

    /**
     * возвращает произведение дроби * входное целое число
     * @param i - целое число
     * @return - дробь
     */
    public Frac mul(int i)
    {
        int nn = this.numerator * i;
        int nd = this.denominator;
        Frac nF = new Frac(nn, nd);
        nF.simple();
        return nF;
    }

    /**
     * возвращает деление дроби / входное целое число
     * @param i - целое число
     * @return - дробь
     */
    public Frac div(int i)
    {
        Frac nF = new Frac(this);
        try
        {
            nF.setFrac(this.numerator, this.denominator * i);
            nF.simple();
        }
        catch (ArithmeticException e1)
        {
            System.out.println("Can't be divide by zero! " + e1.getMessage());
        }
        catch (Exception e2)
        {
            System.out.println("Something wrong. Can't be divide by zero! " + e2.getMessage());
        }
        return nF;

//        if(a == 0)
//        {
//            throw new ArithmeticException("Can't be divide by zero!");
//        } else
//        {
//            nF.setFrac(this.numerator, this.denominator * a);
//            nF.simple();
//        }
//        return nF;
    }

    /**
     * возвращает деление дроби / входной дроби
     * @param frac - входящая дробь
     * @return - дробь
     */
    public Frac div(Frac frac)
    {
        int nn = this.numerator * frac.denominator;
        int nd = this.denominator * frac.numerator;
        Frac nF = new Frac(nn, nd);
        nF.simple();
        return nF;
    }

    /**
     * возвращает деление входное целое число / дроби
     * @param i - целое число
     * @return - дробь
     */
    public Frac intDiv(int i)
    {
        int nn = this.denominator * i;
        int nd = this.numerator;
        Frac nF = new Frac(nn, nd);
        nF.simple();
        return nF;
    }

    /**
     * возвращает дробь как результат возведение в степени mPower нашу дробь
     * @param mPower - целое число, степень
     * @return - дробь
     */
    public Frac power(int mPower)
    {
        int nn = this.numerator;
        int nd = this.denominator;
        nn = (int) Math.pow(nn,mPower);
        nd = (int) Math.pow(nd,mPower);
        Frac nF = new Frac(nn, nd);
        nF.simple();
        return nF;
    }

    /**
     * проверяет если дробь равна с входной дробью
     * @param frac - дробь
     * @return - {@code true} если значения равны, иначе {@code false}
     */
    boolean equals(Frac frac)
    {
        return ((this.numerator) * (frac.denominator) == (frac.numerator) * (this.denominator));
    }

    /**
     * проверяет если дробь больше входной дроби
     * @param frac - дробь
     * @return - {@code true} если дробь больше входящей дроби, иначе {@code false}
     */
    boolean isOver(Frac frac)
    {
        return ((this.numerator) * (frac.denominator) > (frac.numerator) * (this.denominator));
    }

    /**
     * проверяет если дробь меньше  входной дроби
     * @param frac - дробь
     * @return - {@code true} если дробь меньше входящей дроби, иначе {@code false}
     */
    boolean isUnder(Frac frac)
    {
        return ((this.numerator) * (frac.denominator) < (frac.numerator) * (this.denominator));
    }

    /**
     * проверяет если дробь больше и равно входной дроби
     * @param frac - дробь
     * @return - {@code true} если дробь больше или равно входящей дроби, иначе {@code false}
     */
    boolean isOverEqual(Frac frac)
    {
        return ((this.numerator) * (frac.denominator) >= (frac.numerator) * (this.denominator));
    }

    /**
     * проверяет если дробь меньше и равно  входной дроби
     * @param frac - дробь
     * @return - {@code true} если дробь меньше или равно входящей дроби, иначе {@code false}
     */
    boolean isUnderEqual(Frac frac)
    {
        return ((this.numerator) * (frac.denominator) <= (frac.numerator) * (this.denominator));
    }


    //    public int nod(int m, int n)
//    {
//        if (m != 1 && n != 1)
//        {
//            if(m<n)
//            {
//                int z = m;
//                m = n;
//                n = z;
//            }
//            int r = m % n;
//            while (r != 0 && n != 1)
//            {
//                m = n;
//                n = r;
//                r = m % n;
//            }
//            if (n != 0 && n != 1)
////            {
////                this.numerator = this.numerator / n;
////                this.denominator = this.denominator / n;
////            }
//        }
//        return n;
//    }
//
//       /*Переводит из строки обратно в дробь*/
//    public Frac fromString(String str)
//    {
//        this.arr = new String[1];
//        this.arr[0] = null;
//        int infrl = infr.length();
//        this.arr = new String[] {infr};
////        System.out.println(this.arr[0]);
//        int sl = 1;
//        for (int i = 1; i < infr.length(); i++)
//        {
//            if (this.arr[i] == "/")
//            {
//                sl = i;
//            }
//        }
//        System.out.println(this.arr[0]);
//        System.out.println(sl);
//        return null;
//    }
}