package frac;

/**
 * Created by john on 21.08.2016.
 */
public class ArrayFrac
{
    private Frac arr[];

    ArrayFrac() // Create new array of Frac with 1 element that = 0, where numerator = 0, denominator = 1
    {
        this.arr = new Frac[1];
        this.arr[0] = new Frac(0);
    }

    ArrayFrac(int n) // Create new array of Frac with n elements that are = 0/1
    {
        this.arr = new Frac[n];
        for (int i = 0; i < n; i++)
        {
            this.arr[i] = new Frac(0);
        }
    }

    ArrayFrac(Frac[] arrf) // Create new array of Frac with array.length that are = with elements = array
    {
        this.arr = new Frac[arrf.length];
        for (int i = 0; i < arrf.length; i++)
        {
            this.arr[i] = new Frac(arrf[i]);
        }
    }

    ArrayFrac(ArrayFrac arrF) // Clone the ArrayFrac
    {
        this.arr = new Frac[arrF.arr.length];
        for (int i = 0; i < arrF.arr.length; i++)
        {
            this.arr[i] = new Frac(arrF.arr[i]);
        }
    }

    public Frac getFracFromArray(int n) // Get the n's Frac from ArrayFrac
    {
        return this.arr[n];
    }

    public void setFracToNElOfArFr(Frac f, int n) // Set the n's Frac to ArrayFrac
    {
        if (n <= this.arr.length)
        {
            this.arr[n].setFrac(f);
        }
        else
        {
            System.out.println("Sorry, but the n is bigger than this.arr.length");
        }
    }
    public int getLengthOfArrayFrac() /* return the length of the current ArrayFrac */
    {
        return this.arr.length;
    }
    public void setArr(ArrayFrac arrF) /* In the current ArrayFrac set the value of input ArrayFrac */
    {
        /* в тек. массив присваивает значение входного массива, т.е. клонирует класс */
        this.arr = new Frac[arrF.arr.length];
        for (int i = 0; i < arrF.arr.length; i++)
        {
            this.arr[i] = new Frac(arrF.arr[i]);
        }
    }

    public void inArr() /* Input the length of new ArrayFrac & input the elements & input the Frac*/
    {
        int lengthOfArray;
//        Scanner sc = new Scanner(System.in);
//        do {
//            System.out.print("Please, enter the length of the array, it must be a positive number: ");
//            while (!sc.hasNextInt())
//            {
//                System.out.println("That's not a number!");
//                sc.next(); // this is important!
//            }
//            n = sc.nextInt(); // this is the length of array
//        } while (n <= 0);
//        System.out.println("Thank you! Got " + n);
        lengthOfArray = Frac.inPositive();
        this.arr = new Frac[lengthOfArray];//считываем целое число и выделяем место под массив
        for (int i = 0; i < lengthOfArray; i++)
        {
            this.arr[i] = new Frac();
            System.out.println("Please, enter " + i + " element of the array");
            this.arr[i].inFrac();
            System.out.println(i + " element = " + arr[i]);
        }
    }

    public void inArr(int n) /* Input input the elements & input the Frac, where the n is the length of the Array*/
    {
        if (n > 0)
        {
            this.arr = new Frac[n];//считываем целое число и выделяем место под массив
            for (int i = 0; i < n; i++)
            {
                this.arr[i] = new Frac();
                System.out.println("Please, enter " + i + " element of the array");
                this.arr[i].inFrac();
//            System.out.println(i + " element = " + arr[i]);
            }
        } else
        {
            System.out.println("Sorry, the length of the array is less then 1");
        }
    }

    public void printArr() /* Print the ArrayFrac by elements */
    {
        int n = this.arr.length;
//        System.out.print("Your arrayFrac contents " + n + " elements\n");
        for (int i = 0; i < n; i++)
        {
//            System.out.print("The " + i + " element of your Frac:\n");
            this.arr[i].printFrac();
        }
    }

    public Frac maxElofArr() /* calculate the maximum elements of the Current ArrayFrac*/
    {
        int n = this.arr.length; //задаём размер массива
        Frac max = new Frac(this.arr[0]); // Clone 0's element in max

        for (int i = 1; i < n; i++)
        {
            if (max.isUnder(this.arr[i]))
            {
                max.setFrac(this.arr[i]); // Clone max's element in max
            }
        }
        System.out.println("Maximum element of the array = " + max);
        return max;
    }

    public static Frac smaxElofArr(ArrayFrac AF) /* calculate the maximum elements of the input ArrayFrac*/
    {
        int n = AF.arr.length;//задаём размер массива
        Frac smax = new Frac(AF.arr[0]); // Clone 0's element in smax

        for (int i = 1; i < n; i++)
        {
            if (smax.isUnder(AF.arr[i]))
            {
                smax.setFrac(AF.arr[i]); // Clone max's element in smax
            }
        }
        System.out.println("Maximum element of the array = " + smax);
        return smax;
    }

    public Frac sumArr() /* calculate the sum of Frac's in the current Array */
    {
        int n = this.arr.length;
        Frac sum = new Frac(this.arr[0]); // Clone the 0's element to sum
        for (int i = 1; i < n; i++)
        {
            sum = sum.add(this.arr[i]); // Add to the cloned sum the current element
        }
        System.out.println("Summary of elements of the array = " + sum);
        return sum;
    }

    public ArrayFrac addArrFrac(ArrayFrac AF)  /* Return the Add of 2 ArrayFrac */
    {
        int firstArrLen = this.arr.length;//задаём размер 1 массива
        int secArrLen = AF.arr.length;//задаём размер 2 массива

        int arrLen = firstArrLen;//задаём размер конечного массива
        int minLen = secArrLen;
        if (arrLen < secArrLen)
        {
            arrLen = secArrLen;
            minLen = firstArrLen;
        }//высчитываем размер конечного массива

        ArrayFrac arrSum = new ArrayFrac(arrLen);//считываем целое число и выделяем место под конечный массив

        for (int i = 0; i < minLen; i++)
        {
            arrSum.arr[i] = this.arr[i].add(AF.arr[i]);
//            System.out.println("Sum of the " + i + " elements of arrays " + arrs[i]); //считываем целое число
        }

        if (firstArrLen < secArrLen)
        {
            for (int i = minLen; i < arrLen; i++)
            {
                arrSum.arr[i].setFrac(AF.arr[i]);
//                System.out.println("Сумма чисел " + i + "-ых элементов массива равна " + arrs[i]); //считываем целое число
            }
        } else
        {
            for (int i = minLen; i < arrLen; i++)
            {
                arrSum.arr[i].setFrac(this.arr[i]);
//                System.out.println("Сумма чисел " + i + "-ых элементов массива равна " + arrs[i]); //считываем целое число
            }
        }
        return arrSum;
    }
    public void sortCurrentArrayFrac() /* Sort the array by Insertion sort method */
    {
        int iCount = 0, sCount = 0;
        if (this.arr.length > 1)
        {
            for (int j = 1; j < this.arr.length; j++)
            {
                Frac key = new Frac(this.arr[j]);
                int i = j - 1;
                iCount++;
                while (i >= 0 && this.arr[i].isOver(key))
                {
                    this.arr[i+1].setFrac(this.arr[i]);
                    i--;
                    sCount++;
                }
                sCount++;
                this.arr[i+1].setFrac(key);
            }
        }
        System.out.println("Number of iterations: " + iCount);
        System.out.println("Number of changes: " + sCount);
    }
    public ArrayFrac sortArrayFrac(ArrayFrac AF) /* Return the Sorted array by Insertion sort method */
    {
        ArrayFrac Sorted;
        Sorted = new ArrayFrac(AF);
        int ArrLen = Sorted.arr.length;
        if (ArrLen > 1)
        {
            for (int j = 1; j < ArrLen; j++)
            {
                Frac key = new Frac(Sorted.arr[j]);
                int i = j - 1;
                while (i >= 0 && Sorted.arr[i].isOver(key))
                {
                    Sorted.arr[i+1].setFrac(Sorted.arr[i]);
                    i--;
                }
                Sorted.arr[i+1].setFrac(key);
            }
        }
        return Sorted;
    }
}