package frac;

/**
 * @author john
 * @since 23.08.2016.
 */
public class MatrixFrac
{
    private ArrayFrac arr[];

    MatrixFrac() // Create new array of ArrayFrac with 1 element that = 0, where numerator = 0, denominator = 1
    {
        this.arr = new ArrayFrac[1];
        this.arr[0] = new ArrayFrac();
    }

    MatrixFrac(int n) // Create new array of ArrayFrac with n elements that are = 0/1
    {
        this.arr = new ArrayFrac[n];
        for (int i = 0; i < n; i++)
        {
            this.arr[i] = new ArrayFrac();
        }
    }

    MatrixFrac(int n, int m) // Create new array of ArrayFrac with n & m elements that are = 0/1
    {
        this.arr = new ArrayFrac[n];
        for (int i = 0; i < n; i++)
        {
            this.arr[i] = new ArrayFrac(m);
        }
    }

    MatrixFrac(ArrayFrac AF, int n, int m) // Create MatrixFrac from ArrayFrac with n & m elements with values of AF
    {
        int ctr = 0;
        for (int row = 0; row < n; row++)
        {
            for (int col = 0; col < m; col++)
            {
                this.arr[row].getFracFromArray(col).setFrac(AF.getFracFromArray(ctr++));
            }
        }
    }

    public void setMatFrFromArrFr(ArrayFrac AF, int n, int m) // Set to the current MatrixFrac from ArrayFrac with n & m elements with values of AF
    {
        int ctr = 0;
        for (int row = 0; row < n; row++)
        {
            for (int col = 0; col < m; col++)
            {
                this.arr[row].getFracFromArray(col).setFrac(AF.getFracFromArray(ctr++));
            }
        }
    }
    MatrixFrac(ArrayFrac[] arrf) // Create new array of ArrayFrac with array.length that are = with elements = array
    {
        this.arr = new ArrayFrac[arrf.length];
        for (int i = 0; i < arrf.length; i++)
        {
            this.arr[i] = new ArrayFrac(arrf[i]);
        }
    }

    MatrixFrac(MatrixFrac arrAF) // Clone the MatrixFrac
    {
        this.arr = new ArrayFrac[arrAF.arr.length];
        for (int i = 0; i < arrAF.arr.length; i++)
        {
            this.arr[i] = new ArrayFrac(arrAF.arr[i]);
        }
    }

    public void setMatrix(MatrixFrac arrAF) // In the current MatrixFrac set the values of given MatrixFrac
    {
        /* в тек. матрицу присваивает значение входноой матрицы, т.е. клонирует класс */
        this.arr = new ArrayFrac[arrAF.arr.length];
        for (int i = 0; i < arrAF.arr.length; i++)
        {
            this.arr[i] = new ArrayFrac(arrAF.arr[i]);
        }
    }

    public Frac getFracFromIJElOfMatrix(int i, int j)// Get the Frac of the ij's element of the current Matrix
    {
        if (i < this.arr.length && j < this.arr[0].getLengthOfArrayFrac())
        {
            return this.arr[i].getFracFromArray(j);
        } else
        {
            System.out.println("Reached the maximum of Matrix Elements!");
            return null;
        }
    }

    public void setFracToIJElOfMatrix(Frac F, int i, int j)// Set the Frac to the ij's element of the current Marix
    {
        if (i <= this.arr.length && j <= this.arr[0].getLengthOfArrayFrac())
        {
            this.arr[i].getFracFromArray(j).setFrac(F);
        } else
        {
            System.out.println("Reached the maximum of Matrix Elements!");
        }
    }

    public void inMatrix()
    {
        /* Input the length of new ArrayFrac & input the elements & input the Frac */
        int mLengthOfArray;
        int nLengthOfArray;
        mLengthOfArray = Frac.inPositive();
        System.out.println("m values of Matrix is: " + mLengthOfArray);
        nLengthOfArray = Frac.inPositive();
        System.out.println("n values of Matrix is: " + nLengthOfArray);
        this.arr = new ArrayFrac[mLengthOfArray];//считываем целое число и выделяем место под массив
        for (int i = 0; i < mLengthOfArray; i++)
        {
            this.arr[i] = new ArrayFrac(nLengthOfArray);
            System.out.println("Please, enter " + i + " element of the matrix");
            this.arr[i].inArr(nLengthOfArray);
//            System.out.println(i + " element = " + arr[i]);
        }
    }

    public void printMatrix()
    {
        /* Print the MatrixFrac by elements */
        int n = this.arr.length;
        System.out.println("Your matrixFrac contents " + n + " elements:");
        for (int i = 0; i < n; i++)
        {
//            System.out.print("The " + i + " element of your Frac:\n");
            this.arr[i].printArr();
            System.out.println();
        }
    }

    public Frac maxElofMatrix()
    {
        /* calculate the maximum elements of the MatrixFrac*/
        int n = this.arr.length;//задаём размер массива
        Frac max = new Frac(this.arr[0].maxElofArr()); // Clone 0's element in max
        Frac isMax = new Frac();
        for (int i = 1; i < n; i++)
        {
            isMax.setFrac(this.arr[i].maxElofArr());
            if (max.isUnder(isMax))
            {
                max.setFrac(isMax); // Clone max's element in max
            }
        }
        System.out.println("Maximum element of the matrix = " + max);
        return max;
    }

    public Frac sumMatrix()
    {
        /* calculate the sum of Frac's in the current Matrix */
        int n = this.arr.length;
        Frac sum = new Frac(this.arr[0].sumArr()); // Clone the 0's element to sum
        for (int i = 1; i < n; i++)
        {
            sum = sum.add(this.arr[i].sumArr()); // Add to the cloned sum the current element
        }
        System.out.println("Summary of elements of the Matrix = " + sum);
        return sum;
    }

    public void sortCurrentRowsMatrixFrac() /* Sort the MatrixFrac elements in the rows by Insertion sort method */
    {
        int nLengthOfArray = this.arr.length;
        int mLengthOfArray = this.arr[0].getLengthOfArrayFrac();
        if (mLengthOfArray > 1)
        {
            for (int j = 1; j < mLengthOfArray; j++)
            {
                int i = j - 1;
                Frac key = new Frac(this.arr[i].getFracFromArray(j));
                while (i >= 0 && this.arr[i].getFracFromArray(i).isOver(key))
                {
                    this.arr[i].sortCurrentArrayFrac();
                    i--;
                }
            }
        }
    }

    public void sortCurrentMatrixFrac() /* Sort the MatrixFrac elements Insertion sort method */
    {
        int nLengthOfArray = this.arr.length;
        int mLengthOfArray = this.arr[0].getLengthOfArrayFrac();
        ArrayFrac flat;
        flat = new ArrayFrac(nLengthOfArray * mLengthOfArray);
        int ctr = 0;
        for (int row = 0; row < nLengthOfArray; row++)
        {
            for (int col = 0; col < mLengthOfArray; col++)
            {
                flat.setFracToNElOfArFr(this.arr[row].getFracFromArray(col), ctr++);
            }
        }
        flat.sortCurrentArrayFrac();
        ctr = 0;
        for (int row = 0; row < nLengthOfArray; row++)
        {
            for (int col = 0; col < mLengthOfArray; col++)
            {
                this.arr[row].getFracFromArray(col).setFrac(flat.getFracFromArray(ctr++));
            }
        }
    }

    public void sortCurrentMatrixFracCNA() /* Sort the MatrixFrac elements in the rows by Insertion sort method */
    {
        int nLengthOfArray = this.arr.length;
        int mLengthOfArray = this.arr[0].getLengthOfArrayFrac();
        ArrayFrac flat = this.convertMatFrToArrFr();
        flat.sortCurrentArrayFrac();
        this.setMatFrFromArrFr(flat,nLengthOfArray,mLengthOfArray);
    }

    public ArrayFrac convertMatFrToArrFr() // Return new ArrayFrac from current MatrixFrac
    {
        int nLengthOfArray = this.arr.length;
        int mLengthOfArray = this.arr[0].getLengthOfArrayFrac();
        ArrayFrac flat;
        flat = new ArrayFrac(nLengthOfArray * mLengthOfArray);

        int ctr = 0;
        for (int row = 0; row < nLengthOfArray; row++)
        {
            for (int col = 0; col < mLengthOfArray; col++)
            {
                flat.setFracToNElOfArFr(this.arr[row].getFracFromArray(col),ctr++);
            }
        }
        return flat;
    }

    public void sortCurrentMFBM() /* Sort the matrix by Bubble sort method */
    {
        int mLengthOfArray = this.arr.length;
        int nLengthOfArray = this.arr[0].getLengthOfArrayFrac();
        Frac key;
        key = new Frac();
        int iCount = 0, sCount = 0;

        for (int i = 0; i < mLengthOfArray; ++i)
        {
            for (int j = 0; j < nLengthOfArray; ++j)
            {
                for (int m = 0; m < mLengthOfArray; ++m)
                {
                    for (int n = 0; n < nLengthOfArray; ++n)
                    {
                        ++iCount;
                        if (this.getFracFromIJElOfMatrix(m,n).isOver(this.getFracFromIJElOfMatrix(i,j)))
                        {
                            key.setFrac(this.getFracFromIJElOfMatrix(m,n));
                            this.setFracToIJElOfMatrix(this.getFracFromIJElOfMatrix(i,j),m,n);
                            this.setFracToIJElOfMatrix(key,i,j);
                            ++sCount;
                        }
                    }
                }
            }
        }
        System.out.println("Number of iterations: " + iCount);
        System.out.println("Number of changes: " + sCount);
    }

    public void sortCurrentMFBM1() /* Sort the matrix by Bubble sort method */
    {
        int mLengthOfArray = this.arr.length;
        int nLengthOfArray = this.arr[0].getLengthOfArrayFrac();
        Frac key;
        key = new Frac();
        int iCount = 0, sCount = 0;

        int bound = mLengthOfArray * nLengthOfArray;
        int t;
        do
        {
            t = 0;
            for (int j = 0; j < bound-1; j++)
            {
                int k,l,k1,l1;
                k = j / nLengthOfArray;
                l = j % nLengthOfArray;
                k1 = (j+1) / nLengthOfArray;
                l1 = (j+1) % nLengthOfArray;

                if (this.getFracFromIJElOfMatrix(k1,l1).isUnder(this.getFracFromIJElOfMatrix(k,l)))
                {
                    ++iCount;
                    key.setFrac(this.getFracFromIJElOfMatrix(k1,l1));
                    this.setFracToIJElOfMatrix(this.getFracFromIJElOfMatrix(k,l),k1,l1);
                    this.setFracToIJElOfMatrix(key,k,l);
                    t = j+1;
                    ++sCount;
                }
                else
                {
                    ++iCount;
                }
            }
            bound = t;
        }
        while (t != 0 && bound > 1);

//        for (int i = 0; i < mLengthOfArray; ++i)
//        {
//            for (int j = 0; j < nLengthOfArray; ++j)
//            {
//                for (int m = 0; m < mLengthOfArray * nLengthOfArray; ++m)
//                {
//                    int k, l;
//                    k = m / nLengthOfArray;
//                    l = m % nLengthOfArray;
//                        if (this.getFracFromIJElOfMatrix(k,l).isOver(this.getFracFromIJElOfMatrix(i,j)))
//                        {
//                            ++iCount;
//                            key.setFrac(this.getFracFromIJElOfMatrix(k,l));
//                            this.setFracToIJElOfMatrix(this.getFracFromIJElOfMatrix(i,j),k,l);
//                            this.setFracToIJElOfMatrix(key,i,j);
//                            ++sCount;
//                        }
//                        else
//                        {
//                            ++iCount;
//                        }
//                }
//            }
//        }
        System.out.println("Number of iterations: " + iCount);
        System.out.println("Number of changes: " + sCount);
    }

    public void sortCurrentMFBMA() /* Sort the matrix by Advanced Bubble sort method */
    {
        int mLengthOfArray = this.arr.length;
        int nLengthOfArray = this.arr[0].getLengthOfArrayFrac();
        Frac key;
        key = new Frac();
        int iCount = 0, sCount = 0;

        for (int i = 0; i < mLengthOfArray; ++i)
        {
            for (int j = 0; j < nLengthOfArray; ++j)
            {
                intloopM: for (int m = mLengthOfArray - 1; m >= 0; --m)
                {
                    for (int n = nLengthOfArray - 1; n >= 0; --n)
                    {
                        if ((m == i & n < j)
                                | (i - m == 1 & j == 0 & n == mLengthOfArray - 1)
                                | (m < i))
                        {
                            ++iCount;
                            break intloopM;
                        }
                        if (this.getFracFromIJElOfMatrix(m,n).isUnder(this.getFracFromIJElOfMatrix(i,j)))
                        {
                            ++iCount;
                            key.setFrac(this.getFracFromIJElOfMatrix(m,n));
                            this.setFracToIJElOfMatrix(this.getFracFromIJElOfMatrix(i,j),m,n);
                            this.setFracToIJElOfMatrix(key,i,j);
                            ++sCount;
                        }
                        else
                        {
                            ++iCount;
                        }

                    }
                }
            }
        }
        System.out.println("Number of iterations: " + iCount);
        System.out.println("Number of changes: " + sCount);
    }

    public void sortCMFrac() /* Sort the current MatrixFrac by Insertion sort method */
    {
        int iCount = 0, sCount = 0;
        if (this.arr.length > 1)
//        {
//            for (int j = 1; j < this.arr.length; j++)
//            {
//                Frac key = new Frac(this.arr[j]);
//                int i = j - 1;
//                iCount++;
//                while (i >= 0 && this.arr[i].isOver(key))
//                {
//                    this.arr[i+1].setFrac(this.arr[i]);
//                    i--;
//                    sCount++;
//                }
//                sCount++;
//                this.arr[i+1].setFrac(key);
//            }
//        }
            System.out.println("Number of iterations: " + iCount);
        System.out.println("Number of changes: " + sCount);
    }
}
